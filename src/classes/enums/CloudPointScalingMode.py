from enum import Enum


class CloudPointScalingMode(Enum):
    RAW = "raw",
    NORMALIZED = "normalized",
    TRUE_SCALE = "true_scale"
