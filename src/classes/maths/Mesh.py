import scipy
from classes.enums.MeshExportType import MeshExportType
from classes.maths.CloudPoint import CloudPoint
from classes.maths.Triangle import Triangle
from classes.utils.Logger import Logger
from tqdm import tqdm


class Mesh:
    """
    A class for creating a mesh with triangles.

    Attribute
    ----------
    points : list of Point
    triangles : array of triangles

    Methods
    -------
    create_triangles()
        Returns an array of triangles
    export(filepath: str,to: MeshExportType = MeshExportType.OBJECT)
        Exports the triangle array to an obj,stl or ply file
    __to_obj(filepath)
        Create obj file from triangle array
    __to_stl(filepath)
        Not implemented
    __to_ply(filepath)
        Not implemented

    """

    def __init__(self, points: CloudPoint):
        self.points = points
        self.mesh = None
        self.triangles = []

    def create_triangles(self):
        """
        Browse the list of points and create the triangles.

        Returns
        -------
        array of triangles

        """
        Logger.info("Creating triangles for the Mesh")
        # points = self.points.get_grid()
        in_grid = True
        try:
            points = self.points.get_grid()
        except Exception as _:
            points = self.points.get_points()
            in_grid = False

        if not in_grid:
            Logger.warning(
                "Could not retrieve 2D grid from the CloudPoint, applying Delaunay transformation to the CloudPoint. This might take some time..."
            )
            self.triangles = scipy.spatial.Delaunay(points)
            Logger.info(
                f"{len(self.triangles)} Triangles have been created unsing Delaunay algorithm."
            )
            return

        num_points_x = len(points[0])
        num_points_y = len(points)
        triangles_in_zone = []

        for x in tqdm(range(num_points_x - 1), desc="Creating triangles..."):
            for y in range(num_points_y - 1):
                top_left = points[x, y]  # A
                top_right = points[x + 1, y]  # B
                bottom_left = points[x, y + 1]  # C
                bottom_right = points[x + 1, y + 1]  # D
                triangles_in_zone = [
                    Triangle(top_left, bottom_left, top_right),
                    Triangle(top_right, bottom_left, bottom_right),
                ]
                self.triangles.extend(triangles_in_zone)

        Logger.success(f"{len(self.triangles)} Triangles have been created.")

        return self.triangles

    def export(self, filepath: str, to: MeshExportType = MeshExportType.OBJECT):
        """
        Export an obj, stl or ply file

        Parameters
        ----------
        filepath : str
            The file path for saving the data.
        MeshExportType : MeshExportType
            Type of file (obj, stl or ply). Default value : obj
        """
        Logger.info("Export mesh")
        if to is MeshExportType.OBJECT:
            self.__to_obj(filepath)
        elif to is MeshExportType.STEREOLITHOGRAPHY:
            self.__to_stl(filepath)
        elif to is MeshExportType.POLYGONS:
            self.__to_ply(filepath)
        else:
            raise AttributeError(f"to is not a valid MeshExportType, received {to}.")

    def __to_obj(self, filepath):
        """
        Create obj file from triangle array.

        Parameters
        ----------
        filepath : str
            The file path for saving the data.
        """
        with open(filepath, "w") as f:
            # vertex
            for triangle in self.triangles:
                for point in triangle.triangle():
                    f.write(f"v {point.x} {point.y} {point.z}\n")
            # faces
            vertex_count = len(self.triangles) * 3
            for i in range(1, vertex_count + 1, 3):
                f.write(f"f {i} {i + 1} {i + 2}\n")

    def __to_stl(self, filepath):
        # additional process to convert to stl...
        raise NotImplementedError("Implement process for STL export")

    def __to_ply(self, filepath):
        # additional process to convert to ply...
        raise NotImplementedError("Implement process for PLY export")
