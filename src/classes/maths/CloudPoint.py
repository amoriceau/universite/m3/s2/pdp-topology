from __future__ import annotations

import math
import os
from datetime import datetime
from typing import Any

import numpy as np
from classes.enums.CloudPointScalingMode import CloudPointScalingMode
from classes.enums.CloudPointTransformation import CloudPointTransformation
from classes.maths.Point import Point
from classes.utils.Logger import Logger
from numpy import ndarray, dtype
from tqdm import tqdm


class CloudPoint:
    """
    A class for handling and manipulating point cloud data.

    This class provides a structure for storing and manipulating 3D points represented in a cloud. It supports
    operations such as normalization, slicing, and chunking, as well as saving the data back to an XYZ format.

    Attributes
    ----------
    points : list of Point
        A list of Point objects representing the 3D points in the cloud.
    generation_path : str
        The file path where the point cloud data will be saved.
    scale : CloudPointScalingMode
        The scale mode of the point cloud data, indicating whether the data is raw, normalized or adapted to earth size.

    Methods
    -------
    size()
        Returns the number of points in the cloud.
    get_points()
        Returns the list of Point objects.
    set_generation_path(generation_path)
        Sets the file path for saving the point cloud data.
    get_coordinates(clean=False)
        Returns the x, y, and z coordinates of the points in separate lists.
    normalize()
        Normalizes the point cloud data so that each coordinate is scaled between 0 and 1.
    to_xyz(custom_filename=None)
        Saves the point cloud data to an XYZ format file.
    chunk()
        Divides the point cloud into four chunks based on the median of the x and y coordinates.
    deep_chunk(level=1)
        Recursively divides the point cloud into smaller chunks.
    slice(slices, axis)
        Divides the point cloud into slices along the specified axis.
    assemble(chunks)
        Combines multiple chunks of point clouds into a single CloudPoint instance.
    from_points(points, generation_path=None)
        Creates a CloudPoint instance from a list of Point objects.
    coord()
        Returns the coordinates of every point of the object.
    __mul__(other)
        Supports multiplication of the point cloud by a scalar or a 3-tuple to scale the coordinates.
    __repr__()
        Returns a string representation of the CloudPoint instance.

    See Also
    --------
    Point

    Notes
    -----
    The CloudPoint data can be manipulated and analyzed for various applications such as 3D rendering or
    geographical analysis.
    """

    def __init__(
        self,
        x: list[float],
        y: list[float],
        z: list[float] | None = None,
        generation_path: str = None,
        base_scale: CloudPointScalingMode = CloudPointScalingMode.RAW,
        shape: tuple[int, int] = None,
        transformation: CloudPointTransformation = CloudPointTransformation.NONE,
    ):
        """
        Initializes the CloudPoint instance.

        Parameters
        ----------
        x : list of float
            The x coordinates of the points.
        y : list of float
            The y coordinates of the points.
        z : list of float, optional
            The z coordinates of the points. If not provided, defaults to a list of zeros.
        shape: tuple[int, int], optional
            The shape of the grid that maps the points. If not provided, defaults to (len(x), len(y))
        generation_path : str, optional
            The file path for saving the point cloud data. If not provided, a default path is used.
        base_scale : CloudPointScalingMode, optional
            The scale mode of the point cloud data. Defaults to RAW.
        """
        if z is None:
            z = [0.0] * len(x)
        if shape is None:
            shape = (math.floor(math.sqrt(len(x))), math.floor(math.sqrt(len(y))))

        Logger.info("Initializing CloudPoint...")
        points = []
        for i in tqdm(range(len(x)), desc="Creating Points", disable=Logger.muted):
            points.append(Point(i, float(x[i]), float(y[i]), float(z[i])))

        self.points = points
        self.generation_path = generation_path
        self.scale = base_scale
        self.shape = shape
        self.transformation = transformation
        self.true_size = len(points)
        Logger.success("CloudPoint initialized.")
        self.print_info()

    @property
    def size(self) -> int:
        """
        Returns the number of points in the point cloud.

        Returns
        -------
        int
            The number of points in the cloud.
        """
        return len(self.points)

    @property
    def base_generation_path(self) -> str:
        """
        Returns the default file path for saving the point cloud data.

        Returns
        -------
        str
            The default file path.
        """
        return "../data/generation/xyz"

    def print_info(self) -> None:
        """
        Prints information about the CloudPoint.

        This method displays detailed information about the CloudPoint instance.

        Examples
        --------
        Assuming 'cloud' is an instance of a class containing this method, and has been successfully loaded:

            >>> cloud.print_info()

        This will output detailed information about the cloud directly to the console.
        """
        base_mute_status = Logger.muted
        Logger.muted = False
        points = "{:,}".format(self.size).replace(",", " ")
        shape_x = "{:,}".format(self.shape[0]).replace(",", " ")
        shape_y = "{:,}".format(self.shape[1]).replace(",", " ")
        path = (
            self.generation_path if self.generation_path else self.base_generation_path
        )
        Logger.info("CloudPoint informations")
        Logger.table(
            {
                "Size": f"{points} points",
                "Shape": f"{shape_x} x {shape_y}",
                "Generation path": path,
                "Base scale": self.scale,
            }
        )
        Logger.muted = base_mute_status

    def get_points(self) -> list[Point]:
        """
        Returns the list of Point objects representing the points in the cloud.

        Returns
        -------
        list of Point
            The points in the cloud.
        """
        return self.points

    def get_grid(self) -> np.ndarray:
        """
        Creates a 2D grid from the list of points using NumPy.

        Returns
        -------
        np.ndarray
            A 2D NumPy array representation of the points.
        """
        if self.shape is None:
            Logger.error("Shape of the grid is not specified.")

        cols, rows = self.shape
        if cols * rows != len(self.points):
            Logger.error("Number of points does not match the specified shape.")

        grid = np.array(self.points)
        with tqdm(total=cols * rows, desc="Reshaping Grid") as pbar:
            grid = grid.reshape((rows, cols))
            pbar.update(cols * rows)

        return grid

    def set_generation_path(self, generation_path: str) -> None:
        """
        Sets the file path where the point cloud data will be saved.

        Parameters
        ----------
        generation_path : str
            The file path for saving the data.
        """
        self.generation_path = generation_path

    def get_coordinates(
        self, clean=False
    ) -> tuple[
        ndarray[Any, dtype[Any]], ndarray[Any, dtype[Any]], ndarray[Any, dtype[Any]]
    ]:
        """
        Returns the x, y, and z coordinates of the points as separate lists.

        Parameters
        ----------
        clean : bool, optional
            If True, replaces potential None values with zeros in the z coordinates.

        Returns
        -------
        tuple of lists
            The x, y, and z coordinates of the points.
        """
        x = np.array([point.x for point in self.points])
        y = np.array([point.y for point in self.points])
        z = np.array([point.z for point in self.points])

        if clean:
            z = np.array([0.0 if val is None else val for val in z])
        return x, y, z

    def coordinates(self) -> list[float]:
        """
        Returns the coordinates of the point in 3D space

        Returns
        -------
        list[list[float]]
        """
        return [p.coordinates() for p in self.points]

    def normalize(self) -> None:
        """
        Normalizes the point cloud data.

        Scales the x, y, and z coordinates of each point so that they are in the range [0, 1].
        """
        x, y, z = self.get_coordinates()

        min_x, max_x = min(x), max(x)
        min_y, max_y = min(y), max(y)
        min_z, max_z = min(z), max(z)

        normalized_points = []

        for point in tqdm(
            self.points, desc="Normalizing the CloudPoint", disable=Logger.muted
        ):
            normalized_x = (point.x - min_x) / (max_x - min_x) if max_x > min_x else 0.0
            normalized_y = (point.y - min_y) / (max_y - min_y) if max_y > min_y else 0.0
            normalized_z = (point.z - min_z) / (max_z - min_z) if max_z > min_z else 0.0
            normalized_points.append(Point(normalized_x, normalized_y, normalized_z))

        self.points = normalized_points
        self.scale = CloudPointScalingMode.NORMALIZED
        Logger.info("CloudPoint have been normalized.")

    def to_xyz(self, custom_filename: str = None) -> None:
        """
        Saves the point cloud data to an XYZ format file.

        Parameters
        ----------
        custom_filename : str, optional
            The name of the file to save the data to. If not provided, a timestamp is used.
        """
        Logger.info("Creating .xyz file from CloudPoint.")
        timestamp = datetime.now().strftime("%Y%m%d_%H_%M_%S")
        filename = custom_filename if custom_filename else timestamp
        filename += ".xyz"

        x, y, z = self.get_coordinates(clean=True)
        data = np.column_stack((x, y, z))

        path = (
            self.generation_path if self.generation_path else self.base_generation_path
        )
        filepath = os.path.join(path, filename)

        np.savetxt(filepath, data, fmt="%f", delimiter=" ")
        Logger.success(f"File has been created at {filepath}")

    def chunk(self) -> tuple[CloudPoint, CloudPoint, CloudPoint, CloudPoint]:
        """
        Divides the point cloud into four quadrants (chunks) based on the median values of x and y coordinates.

        The point cloud is divided into top-left, top-right, bottom-left, and bottom-right quadrants. This division
        is based on the median values of the x and y coordinates of the points in the cloud. Each quadrant is then
        returned as a separate CloudPoint instance.

        Returns
        -------
        tuple of CloudPoint
            A tuple containing four CloudPoint instances corresponding to the top-left, top-right, bottom-left,
            and bottom-right quadrants of the original point cloud. In this order.

        Example
        -------
            >>> cloud_point = CloudPoint(x_coordinates, y_coordinates, z_coordinates)
            >>> chunks = cloud_point.chunk()
            >>> for chunk in chunks:
            >>>     print(chunk.size)
        This will output the number of points in each quadrant.

        Notes
        -----
        This method is particularly useful for spatial analysis and for operations that may require processing
        different sections of the point cloud separately. The division based on median values ensures a balanced
        split of the dataset, but does not guarantee equal numbers of points in each quadrant if there are ties
        at the median.
        """
        Logger.info("Chunking CloudPoint into quadrants.")
        x, y, _ = self.get_coordinates()

        median_x = np.median(x)
        median_y = np.median(y)

        points_tl = []  # Top Left
        points_tr = []  # Top Right
        points_bl = []  # Bottom Left
        points_br = []  # Bottom Right

        shape_x = self.shape[0] / 4
        shape_y = self.shape[1] / 4
        additional_x_shape_required = False
        additional_y_shape_required = False

        if shape_x % 1 != 0:
            additional_x_shape_required = True
            shape_x = math.floor(shape_x)

        if shape_y % 1 != 0:
            additional_y_shape_required = True
            shape_y = math.floor(shape_y)

        for point in tqdm(
            self.points, "Point repartition in quadrants", disable=Logger.muted
        ):
            if point.x <= median_x and point.y > median_y:
                points_tl.append(point)
            elif point.x > median_x and point.y > median_y:
                points_tr.append(point)
            elif point.x <= median_x and point.y <= median_y:
                points_bl.append(point)
            elif point.x > median_x and point.y <= median_y:
                points_br.append(point)

        shape_tl = (shape_x, shape_y)
        shape_tr = (shape_x + 1 if additional_x_shape_required else shape_x, shape_y)
        shape_bl = (shape_x, shape_y + 1 if additional_y_shape_required else shape_y)
        shape_br = (
            shape_x + 1 if additional_x_shape_required else shape_x,
            shape_y + 1 if additional_y_shape_required else shape_y,
        )

        cloud_tl = CloudPoint.from_points(
            points_tl, shape=shape_tl, transformation=CloudPointTransformation.CHUNKED
        )
        cloud_tr = CloudPoint.from_points(
            points_tr, shape=shape_tr, transformation=CloudPointTransformation.CHUNKED
        )
        cloud_bl = CloudPoint.from_points(
            points_bl, shape=shape_bl, transformation=CloudPointTransformation.CHUNKED
        )
        cloud_br = CloudPoint.from_points(
            points_br, shape=shape_br, transformation=CloudPointTransformation.CHUNKED
        )

        Logger.success("CloudPoint chunked.")
        return cloud_tl, cloud_tr, cloud_bl, cloud_br

    def deep_chunk(
        self, cloud: CloudPoint | None = None, level: int = 1
    ) -> list[CloudPoint]:
        """
        Recursively divides the point cloud into smaller chunks based on the specified depth level.

        This method splits the point cloud into quadrants (top-left, top-right, bottom-left, bottom-right) and then
        recursively applies the same division process to each chunk until the specified recursion depth (level) is reached.
        This process is used for more granular analysis or processing of the point cloud in smaller, more manageable sections.
        The number of chunks produced is 4^level

        Parameters
        ----------
        cloud : CloudPoint, optional
            The point cloud to be chunked. If not specified, the method uses the instance itself (self).
        level : int, optional
            The depth of recursion for chunking the point cloud. A level of 0 means no further chunking is performed
            beyond the initial split. A level of 1 means each of the initial chunks is split once more, and so on.
            Defaults to 1.

        Returns
        -------
        list of CloudPoint
            A list of CloudPoint instances representing the subdivided chunks of the original point cloud.
            The number of CloudPoint instances returned equals 4^level.

        Examples
        --------
        Assuming 'cloud' is an instance of CloudPoint containing multiple points:

            >>> subdivided_clouds = cloud.deep_chunk(level=2)
            This would divide the cloud into 16 (4^2) smaller chunks.

        See Also
        --------
        chunk: That split a Cloud in four quadrants

        Notes
        -----
        The method is designed for dividing point clouds that represent large areas or volumes into smaller segments
        for detailed analysis or parallel processing. Each chunk is a separate CloudPoint instance and can be manipulated
        independently.

        Be awara that you can chunk a CloudPoint so deeply most of the returned CloudPoint will have no points.

        Warnings
        --------
        Be aware that this method might raise errors if the data can not be chunked. It might also consume a lot of CPU
        depending on the depth of recursion.
        """
        if cloud is None:
            Logger.info("Deep-chunk in progress...")
            cloud = self

        if level == 0:
            return [cloud]

        chunks = cloud.chunk()
        sub_chunks = []
        for chunk in chunks:
            sub_chunks.extend(self.deep_chunk(chunk, level - 1))

        Logger.success(f"CloudPoint chunked in {len(sub_chunks)} chunks.")
        return sub_chunks

    def slice(self, slices: int, axis: str) -> list[CloudPoint]:
        """
        Divides the point cloud into a specified number of slices along a given axis.

        This method partitions the point cloud into evenly distributed slices based on the number of unique
        values along the specified axis ('X' or 'Y'). Each slice contains a subset of points from the original
        cloud that fall within the slice's range. The method is useful for segmenting the point cloud for
        parallel processing or for focused analysis on specific spatial sections.

        Parameters
        ----------
        slices : int
            The number of slices to divide the point cloud into. Must be a positive integer.
        axis : str
            The axis along which to slice the point cloud. Must be 'X' or 'Y' (case-insensitive).

        Returns
        -------
        list of CloudPoint
            A list containing the resulting CloudPoint objects after slicing. The length of the list
            will be equal to the specified number of slices.

        Raises
        ------
        ValueError
            If the specified axis is not 'X' or 'Y'.

        Examples
        --------
        Assuming 'cloud' is an instance of CloudPoint containing multiple points:

            >>> sliced_clouds = cloud.slice(slices=5, axis='X')
            This would divide the cloud into 5 slices along the X-axis.

        Notes
        -----
        The method ensures that each slice contains a roughly equal number of unique points along the specified axis.
        Points at the boundaries of slices are assigned to a single slice based on their exact coordinates.
        """
        if axis.upper() not in ["X", "Y"]:
            Logger.error(f"Axis must be 'X' or 'Y', '{axis.upper()}' given.")

        coordinates = self.get_coordinates(clean=True)
        axis_index = 0 if axis.upper() == "X" else 1

        unique_vals = sorted(set(coordinates[axis_index]))
        num_unique = len(unique_vals)
        points_per_slice = round(num_unique / slices)

        static_shape_axis_base = (
            self.shape[0] if axis == "X" else self.shape[1]
        ) / slices
        additional_axis_shape_required = False
        if static_shape_axis_base % 1 != 0:
            additional_axis_shape_required = True
            static_shape_axis_base = math.floor(static_shape_axis_base)

        cloud_slices = []
        for i in tqdm(range(slices), desc="Slicing CloudPoint", disable=Logger.muted):
            start_val = unique_vals[i * points_per_slice]
            end_val = unique_vals[min((i + 1) * points_per_slice, num_unique) - 1]

            if additional_axis_shape_required and i == slices - 1:
                static_shape_axis_base = static_shape_axis_base + 1

            if axis.upper() == "X":
                shape = (static_shape_axis_base, self.shape[1])
                slice_points = [p for p in self.points if start_val <= p.x <= end_val]
            else:
                shape = (self.shape[0], static_shape_axis_base)
                slice_points = [p for p in self.points if start_val <= p.y <= end_val]

            transformation = CloudPointTransformation.SLICED_X
            if axis == "Y":
                transformation = CloudPointTransformation.SLICED_Y

            cloud_slices.append(
                CloudPoint.from_points(
                    slice_points,
                    shape,
                    self.generation_path,
                    transformation=transformation,
                )
            )

        Logger.success("Slicing complete.")
        return cloud_slices

    @staticmethod
    def assemble(
        chunks: tuple[CloudPoint, CloudPoint, CloudPoint, CloudPoint]
    ) -> CloudPoint:
        """
        Combines multiple CloudPoint chunks into a single CloudPoint instance.

        This **static** method takes a list of CloudPoint instances (chunks) and merges them into a single
        CloudPoint instance. The points from each chunk are combined and sorted based on their original
        IDs to maintain spatial integrity and order. This method is useful for reassembling a point cloud
        that has been previously divided into chunks for processing or analysis.

        Parameters
        ----------
        chunks : tuple of CloudPoint
            A tuple containing CloudPoint instances to be combined into a single cloud.

        Returns
        -------
        CloudPoint
            A new CloudPoint instance containing all points from the input chunks, sorted by their IDs.

        Examples
        --------
        Assuming 'chunks' is a list of CloudPoint instances:

            >>> combined_cloud = CloudPoint.assemble(chunks)
            This creates a new CloudPoint instance that contains all points from the chunks, sorted by ID.

        Notes
        -----
        The method assumes that each point has a unique ID across all chunks for proper sorting and assembly.
        If point IDs are not unique across chunks, the final order of points in the assembled CloudPoint may not
        reflect the original spatial configuration.
        """
        Logger.info(
            f"Assembling {len(chunks)} CloudPoints chunks into a single CloudPoint instance."
        )
        new_points = []
        shape = (0, 0)

        for chunk in tqdm(chunks, desc="Assembling CloudPoints"):
            if chunk.transformation is CloudPointTransformation.CHUNKED:
                shape = (shape[0] + chunk.shape[0], shape[1] + chunk.shape[1])
            elif chunk.transformation is CloudPointTransformation.SLICED_X:
                shape = (shape[0] + chunk.shape[0], chunk.shape[1])
            elif chunk.transformation is CloudPointTransformation.SLICED_X:
                shape = (chunk.shape[0], shape[1] + chunk.shape[1])

            new_points.extend(chunk.points)

        Logger.success("New CloudPoint created from all chunks.")
        return CloudPoint.from_points(
            sorted(new_points, key=lambda p: p.id), shape=shape
        )

    @staticmethod
    def from_points(
        points: list[Point],
        shape: tuple[int, int],
        generation_path: str = None,
        transformation: CloudPointTransformation = CloudPointTransformation.NONE,
    ) -> CloudPoint:
        """
        Creates a CloudPoint instance from a list of Point objects.

        This static method takes a list of Point instances and optionally a path where the generated data can be saved.
        It constructs a new CloudPoint instance, sorts the points based on their IDs, and sets the provided generation path
        for the new CloudPoint instance. This method is typically used to create a CloudPoint object from a set of points
        that have been generated or manipulated outside the scope of an existing CloudPoint instance.

        Parameters
        ----------
        transformation
        points : list[Point]
            A list of Point instances to be included in the new CloudPoint object. The points will be sorted by their ID
            attribute to maintain a consistent order.
        shape: tuple[int, int]
            Defines the number of columns and rows the CloudPoint instance may have
        generation_path : str, optional
            The file path where the point cloud data might be saved in the future. This is not used to save data immediately
            but sets the path for future save operations. If not provided, the CloudPoint instance will use a default
            generation path.

        Returns
        -------
        CloudPoint
            A new CloudPoint instance containing the provided points, sorted by their IDs.

        Examples
        --------
        Assuming 'points' is a list of Point instances:

            >>> new_cloud = CloudPoint.from_points(points,(100,100) '/path/to/save/data')
            This creates a new CloudPoint instance containing all provided points using a 100x100 shape,
            with the
            data save path set to
            '/path/to/save/data'.

        Notes
        -----
        The method ensures all points in the returned CloudPoint instance are sorted by their ID to maintain spatial
        consistency. The 'generation_path' is optional and can be set later using the 'set_generation_path' method
        of the CloudPoint instance.
        """
        cols, rows = shape
        if len(points) != cols * rows:
            Logger.error(
                f"The given shape {shape} is not compatible with the points provided "
                f"{len(points)} points.",
                throws=False,
            )

        cp = CloudPoint(
            [], [], [], generation_path, shape=shape, transformation=transformation
        )
        cp.points = sorted(points, key=lambda p: p.id)
        cp.true_size = len(points)
        return cp

    def coords(self):
        return [p.coord() for p in self.points]

    def __mul__(
        self, other: float | int | tuple[float | int, float | int, float | int]
    ) -> CloudPoint:
        if isinstance(other, (int, float)):
            # Multiplication par un scalaire
            new_points = [point * other for point in self.points]
        elif isinstance(other, tuple) and len(other) == 3:
            # Multiplication par un tuple (x, y, z)
            new_points = [point * other for point in self.points]
        else:
            raise ValueError("Can only multiply by int, float, or a 3-tuple")

        x, y, z = zip(*[(p.x, p.y, p.z) for p in new_points])
        return CloudPoint(x, y, z)

    def __eq__(self, other):
        if not isinstance(other, CloudPoint):
            return NotImplemented
        if len(self.points) != len(other.points):
            return False
        return all(
            self_point == other_point
            for self_point, other_point in zip(self.points, other.points)
        )

    def __repr__(self):
        return (
            f"CloudPoint(size={self.size},shape={self.shape},generation_path="
            f"{self.base_generation_path if self.generation_path is None else self.generation_path})"
        )

    def __str__(self):
        return self.print_info()
