import numpy as np

from classes.enums.CloudPointScalingMode import CloudPointScalingMode
from classes.maths.CloudPoint import CloudPoint
from classes.maths.SpatialMetrics import SpatialMetrics
from utils.decorators import spatial_data_required


class XYZHandler:
    """
    A handler class for .xyz files used for 3D point cloud data processing.

    This class provides utilities for reading, processing, and handling 3D spatial data typically
    stored in XYZ file formats. It is designed to facilitate the manipulation and analysis of
    point cloud data for applications such as 3D mapping, GIS, and scientific visualization.

    Attributes
    ----------
    filepath : str
        Path to the .xyz file containing the point cloud data.
    dataset : numpy.ndarray
        The loaded dataset containing all point data from the .xyz file.
    x, y, z : SpatialMetrics
        Spatial metrics objects for managing the spatial data's properties and statistics.

    Methods
    -------
    read_data()
        Reads the .xyz data from the file and initializes the x, y, and z attributes with the data.
    update()
        Updates the spatial properties and statistics based on the current dataset state.
    create_cloud_point(scale=CloudPointScalingMode.RAW)
        Generates a CloudPoint object from the current spatial data using the specified scaling mode.

    Notes
    -----
    This class relies on NumPy for data management and assumes that the input XYZ files are formatted
    correctly, with each line containing three numeric values separated by spaces or commas.
    """

    def __init__(self, filepath):
        self.filepath = filepath
        self.dataset = None
        self.x = SpatialMetrics()
        self.y = SpatialMetrics()
        self.z = SpatialMetrics()
        self.read_data()

    def read_data(self):
        """
        Reads the point cloud data from the .xyz file and updates the x, y, and z attributes.

        This method loads the dataset from the specified file path, assuming a standard XYZ format
        (three numeric values per line, representing x, y, and z coordinates). The data is stored
        in NumPy arrays within the SpatialMetrics instances for each axis.

        Note
        ----
        This method automatically updates the spatial metrics based on the loaded data.
        """
        if self.x.range and self.y.range and self.z.range:
            return

        self.dataset = np.loadtxt(self.filepath)
        self.x.data = self.dataset[:, 0]
        self.y.data = self.dataset[:, 1]
        self.z.data = self.dataset[:, 2]

        self.update()

    def update(self):
        """
        Updates the spatial metrics for the x, y, and z attributes based on the current dataset.

        This method recalculates the range, mean, and other statistical properties for each spatial
        dimension based on the current data. This is essential for maintaining accurate spatial
        information and metrics after any changes to the data.

        See Also
        ----
        SpacialMetrics
        """
        self.x.update()
        self.y.update()
        self.z.update()

    @spatial_data_required
    def create_cloud_point(self, scale=CloudPointScalingMode.RAW):
        """
        Creates a CloudPoint object from the spatial data according to the specified scaling mode.

        This method constructs a CloudPoint object, which is a structured representation of the 3D
        point cloud data. The scaling mode determines how the data is scaled or normalized before
        being converted into the CloudPoint format.

        Parameters
        ----------
        scale : CloudPointScalingMode, optional
            The scaling mode to apply to the point cloud data. Options include RAW (default),
            where no scaling is applied, and NORMALIZED, where data is scaled to a 0-1 range.

        Returns
        -------
        CloudPoint
            An instance of the CloudPoint class containing the processed point cloud data.

        See Also
        --------
        CloudPoint

        Note
        ----
        The spatial_data_required decorator ensures that this method is only called if spatial
        data is present and valid.
        """
        x, y, z = [], [], []
        if scale == CloudPointScalingMode.NORMALIZED:
            x, y, z = self.x.normalized, self.y.normalized, self.z.normalized
        elif scale == CloudPointScalingMode.RAW or scale is None:
            x, y, z = self.x.data, self.y.data, self.z.data

        return CloudPoint(x, y, z)
