import numpy as np
from classes.utils.Colors import Colors
from classes.utils.Logger import Logger
from tqdm import tqdm  # Assurez-vous d'avoir installé tqdm


def check_file_status(func):
    """
    Decorator to check if the dataset file is open before executing the function.

    This decorator is designed to be used with methods that require the dataset file to be
    open before proceeding. If the dataset is not open, it raises an Exception.

    Parameters
    ----------
    func : function
        The function to be wrapped by the decorator.

    Returns
    -------
    function
        The wrapper function that includes the check.

    Raises
    ------
    Exception
        If the dataset file is not open.

    Examples
    --------
    @check_file_status
    def some_method(self):
        # The method
    """

    def wrapper(self, *args, **kwargs):
        if not self.dataset:
            raise Exception("Le fichier n'est pas ouvert.")
        return func(self, *args, **kwargs)

    return wrapper


def spatial_data_required(func):
    """
    Decorator to ensure that spatial data is loaded before executing the function.

    This decorator checks if the dataset attribute of the instance contains data.
    It supports datasets that are numpy arrays, lists, or file-like objects. If no data
    is present, it raises a ValueError. If the dataset is a file-like object, it attempts
    to read from it to confirm data presence.

    Parameters
    ----------
    func : function
        The function to be wrapped by the decorator.

    Returns
    -------
    function
        The wrapper function that checks for data before executing the function.

    Raises
    ------
    ValueError
        If there is no data loaded in the dataset.

    Examples
    --------
    @spatial_data_required
    def process_data(self):
        # The method
    """

    def wrapper(self, *args, **kwargs):
        has_data = False
        if isinstance(self.dataset, np.ndarray):
            has_data = self.dataset.size > 0
        elif isinstance(self.dataset, list):
            has_data = len(self.dataset) > 0
        elif hasattr(self.dataset, "read"):
            try:
                self.dataset.read(1)
                has_data = True
            except Exception as e:
                Logger.error(e, throws=False)
                pass

        if not has_data:
            raise ValueError("Dataset must be loaded first.")
        self.read_data()
        return func(self, *args, **kwargs)

    return wrapper


def update_spatial_data(func):
    """
    Decorator for updating spatial data attributes of a class instance before executing the function.

    This decorator calls the `update` method of the instance before executing the function. It is intended
    for use with methods that modify spatial data, ensuring that all spatial metrics are up-to-date.

    Parameters
    ----------
    func : function
        The function to be wrapped by the decorator.

    Returns
    -------
    function
        The wrapper function that updates spatial data before executing the function.

    Examples
    --------
    @update_spatial_data
    def modify_data(self):
        # The method
    """

    def wrapper(self, *args, **kwargs):
        self.update()
        return func(self, *args, **kwargs)

    return wrapper


def progress(label="Processing", total=100, interval=0.1):
    """
    Decorator for displaying a progress bar

    This decorator calls the `update` method of the instance before executing the function. It is intended
    for use with methods that modify spatial data, ensuring that all spatial metrics are up-to-date.

    Parameters
    ----------
    label : str, optional
        The label to display for the progress bar
    total : int,  optional
        The total splits of the progress bar
    interval : float, optional
        The interval between the refresh of the bar (in seconds)
    func : function
        The function to be wrapped by the decorator.

    Returns
    -------
    function
        The wrapper function that updates spatial data before executing the function.

    Examples
    --------
    @update_spatial_data
    def modify_data(self):
        # The method
    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            Logger.info(f"{label} started.")
            if total is not None:
                for _ in tqdm(
                    range(total),
                    desc=f"{Colors.BOLD}{Colors.HEADER}{label}{Colors.END}",
                    colour="green",
                    mininterval=interval,
                    disable=Logger.muted,
                ):
                    result = func(*args, **kwargs)
            else:
                result = func(*args, **kwargs)
            Logger.success(f"{label} finished.")
            return result

        return wrapper

    return decorator
