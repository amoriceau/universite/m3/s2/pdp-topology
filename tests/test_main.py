import os
import unittest
import sys

sys.path.append('./src')
if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(__file__))
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover(start_dir=current_dir, pattern='test_*.py')

    test_runner = unittest.TextTestRunner(verbosity=2)
    # test_runner.run(test_suite)
    
    ret = not test_runner.run(test_suite).wasSuccessful()
    sys.exit(ret)
