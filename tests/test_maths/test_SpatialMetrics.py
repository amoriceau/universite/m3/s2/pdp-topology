import unittest

import numpy as np

from classes.maths.SpatialMetrics import SpatialMetrics


class TestSpatialMetrics(unittest.TestCase):

    def test_empty_initialization(self):
        metrics = SpatialMetrics()
        self.assertIsNone(metrics.size)
        self.assertIsNone(metrics.min)
        self.assertIsNone(metrics.max)
        self.assertIsNone(metrics.range)
        self.assertIsNone(metrics.normalized)
        self.assertIsNone(metrics.mid)

    def test_update_with_data(self):
        data = np.array([1, 2, 3, 4, 5])
        metrics = SpatialMetrics(data)
        metrics.update()
        self.assertEqual(metrics.size, 5)
        self.assertEqual(metrics.min, 1)
        self.assertEqual(metrics.max, 5)
        self.assertEqual(metrics.range, 4)
        np.testing.assert_array_equal(
            metrics.normalized, np.array([0.0, 0.25, 0.5, 0.75, 1.0])
        )
        self.assertEqual(metrics.mid, 3.0)

    def test_update_with_empty_data(self):
        metrics = SpatialMetrics([])
        metrics.update()
        self.assertEqual(metrics.size, 0)
        self.assertIsNone(metrics.min)
        self.assertIsNone(metrics.max)
        self.assertIsNone(metrics.range)
        self.assertIsNone(metrics.normalized)
        self.assertIsNone(metrics.mid)


if __name__ == "__main__":
    unittest.main()
