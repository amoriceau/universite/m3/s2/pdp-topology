from enum import Enum


class Visualizations(Enum):
    TWO_DIMENSIONS = "2d"
    THREE_DIMENSIONS = "3d"
