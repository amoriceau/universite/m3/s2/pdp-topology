import unittest

from classes.maths.Point import Point


class TestPoint(unittest.TestCase):
    def test_initialization(self):
        p = Point(1, 1.0, 1.0, 1.0)
        self.assertEqual(p.id, 1)
        self.assertEqual(p.x, 1.0)
        self.assertEqual(p.y, 1.0)
        self.assertEqual(p.z, 1.0)

    def test_multiplication_scalar(self):
        p = Point(1, 1.0, 2.0, 3.0)
        p_multiplied = p * 2
        self.assertEqual(p_multiplied.x, 2.0)
        self.assertEqual(p_multiplied.y, 4.0)
        self.assertEqual(p_multiplied.z, 6.0)

    def test_multiplication_tuple(self):
        p = Point(1, 1.0, 2.0, 3.0)
        p_multiplied = p * (2, 3, 4)
        self.assertEqual(p_multiplied.x, 2.0)
        self.assertEqual(p_multiplied.y, 6.0)
        self.assertEqual(p_multiplied.z, 12.0)

    def test_coordinated(self):
        p = Point(1, 1.0, 2.0, 3.0)
        coords = p.coordinates()

        self.assertEqual([1.0, 2.0, 3.0], coords)

    def test_equity(self):
        p = Point(1, 1.0, 2.0, 3.0)
        p_clone = Point(1, 1.0, 2.0, 3.0)

        self.assertEqual(p, p_clone)


if __name__ == "__main__":
    unittest.main()
