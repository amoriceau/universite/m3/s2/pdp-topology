import os

import numpy
import rasterio
from classes.enums.CloudPointScalingMode import CloudPointScalingMode
from classes.maths.CloudPoint import CloudPoint
from classes.maths.SpatialMetrics import SpatialMetrics
from classes.utils.Logger import Logger
from tqdm import tqdm
from utils.constants import (
    BASE_DEGREE,
    M_PER_DEGREE_LAT,
    M_PER_DEGREE_LON,
    MAX_ALLOWED_RESOLUTION_DIMINUTION,
)
from utils.decorators import (
    check_file_status,
    spatial_data_required,
    update_spatial_data,
)
from utils.visualization import calculate_scale


class GeoTIFFHandler:
    """
    A handler class for GeoTIFF files used for geospatial data processing.

    This class provides utilities for opening, reading, and processing data from GeoTIFF files,
    commonly used in GIS and remote sensing applications. It supports reading geospatial data,
    extracting raster bands, and converting geographic coordinates into real-world metrics.

    Attributes
    ----------
    filepath : str
        Path to the GeoTIFF file to be handled.
    filename : str
        Name of the file without path and extension.
    dataset : rasterio.io.DatasetReader
        The opened dataset reader object from rasterio library, representing the GeoTIFF file.
    x, y, z : SpatialMetrics
        Spatial metrics for the dataset, including min, max, and data values for each axis.
    spatial_resolution : tuple
        The spatial resolution (density) of the dataset in meters (width, height).
    transform : list
        Affine transformation coefficients used for geographic transformations.
    y_meters, x_meters : float
        Real-world spatial dimensions of the dataset based on geographic location. Used as coefficients for true scale transformation

    Methods
    -------
    open()
        Opens the GeoTIFF file and initializes the dataset.
    close()
        Closes the opened GeoTIFF dataset and releases resources.
    print_info()
        Prints detailed information about the currently opened geospatial dataset.
    read_band(band_number=1)
        Reads and returns data from a specific band of the dataset.
    read_data(band_number=1)
        Reads data from a specified band and updates the instance's X, Y, Z properties.
    update()
        Updates spatial properties and transformations based on current dataset state.
    create_cloud_point(scale=CloudPointScalingMode.RAW)
        Generates a cloud point object from the spatial data.
    normalize_scales()
        Normalizes the spatial scale of the dataset's attributes.
    true_scale()
        Converts spatial data coordinates from latitude and longitude to meters.
    set_resolution_diminution_factor()
        Sets the resolution for downscaling
    reset()
        Resets the CloudPoint dataset

    Notes
    -----
    This class is dependent on the `rasterio` library for reading and handling GeoTIFF files.
    It is designed to facilitate the manipulation and analysis of geospatial data for GIS applications.
    """

    def __init__(self, filepath: str):
        self.filepath = filepath
        self.filename = os.path.basename(filepath).split(".", 1)[0]
        self.dataset = None
        self.x = SpatialMetrics()
        self.y = SpatialMetrics()
        self.z = SpatialMetrics()
        self.spatial_resolution = None
        self.transform = None
        self.y_meters = None
        self.x_meters = None
        self.resolution_diminution_factor = 1

    def open(self) -> None:
        """
        Opens the dataset associated with the instance's file path.

        This method utilizes the rasterio library to open a geospatial dataset from a file. The file path
        is expected to be set prior to calling this method and should point to a valid geospatial dataset
        format supported by rasterio (e.g., GeoTIFF). Once opened, the dataset is stored in the instance's
        'dataset' attribute for further processing and analysis.

        Example
        -------
        Assuming an instance 'geo_handler' of a class that has this 'open' method and a valid 'filepath':

            >>> geo_handler = GeoTIFFHandler('path/to/geospatial/data.tif')
            >>> geo_handler.open()
            >>> print(type(geo_handler.dataset))
            <class 'rasterio.io.DatasetReader'>


        """
        self.dataset = rasterio.open(self.filepath)
        Logger.success(f"{self.filename} has been opened successfully.")

    def close(self) -> None:
        """
        Closes the open dataset associated with this instance.

        This method safely closes the dataset previously opened with the `open` method, ensuring that all
        resources are released properly. This is particularly important for maintaining data integrity and
        freeing up system resources. It checks if the 'dataset' attribute is not None (indicating that a
        dataset is currently open) and then calls the `close` method on the dataset.

        It's recommended to call this method once all data processing is complete, especially before
        deleting the object or exiting the application to avoid data loss and resource leaks.

        Examples
        --------
        Assuming 'geo_handler' is an instance of the class and has an open dataset:

            >>> geo_handler.close()

        Note
        ----
        After calling this method, the dataset cannot be used for further operations unless it is
        reopened with the `open` method. Attempting to access or manipulate the dataset after closure
        will lead to errors.
        """
        if self.dataset:
            self.dataset.close()
            Logger.success(f"{self.filename} has been closed successfully.")

    @check_file_status
    def print_info(self) -> None:
        """
        Prints information about the currently opened geospatial dataset.

        This method displays detailed information about the dataset that has been loaded into the instance.

        Note
        ----
        The method is decorated with check_file_status, which ensures that the dataset is properly loaded and
        available before attempting to print its information. This helps prevent errors and ensures the availability
        of data.

        Examples
        --------
        Assuming 'geo_handler' is an instance of a class containing this method, and a dataset has been successfully
        loaded:

            >>> geo_handler.print_info()

        This will output detailed information about the dataset directly to the console.
        """
        base_mute_status = Logger.muted
        Logger.muted = False
        dx, dy = self.dataset.res

        base_infos = {
            "Filename": self.filename,
            "Bands": self.dataset.count,
            "Downscale factor": f"x{self.resolution_diminution_factor}",
        }

        if self.resolution_diminution_factor != 1:
            x_points = self.dataset.width
            y_points = self.dataset.height
            points = x_points * y_points
            points = "{:,}".format(points).replace(",", " ")
            x_points_scaled = self.dataset.width // self.resolution_diminution_factor
            y_points_scaled = self.dataset.height // self.resolution_diminution_factor
            points_scaled = x_points_scaled * y_points_scaled
            points_scaled = "{:,}".format(points_scaled).replace(",", " ")
            base_infos["Dimensions (original)"] = f"{x_points} x {y_points}"
            base_infos["Dimensions (downscaled)"] = (
                f"{x_points_scaled} x {y_points_scaled}"
            )
            base_infos["Points in cloud (original)"] = f"{points} points"
            base_infos["Points in cloud (downscaled)"] = f"{points_scaled} points"
        else:
            x_points = self.dataset.width
            y_points = self.dataset.height
            points = x_points * y_points
            points = "{:,}".format(points).replace(",", " ")
            base_infos["Dimensions"] = f"{x_points} x {y_points}"
            base_infos["Points in cloud"] = f"{points} points"

        Logger.info("GeotiffHandler file informations:")
        Logger.table(base_infos)

        Logger.table(
            {
                "Coordinates system": self.dataset.crs,
                "Base resolution": f"{dx} * {dy}",
                "Real-World Res. (pts/km)": f"{dx * M_PER_DEGREE_LON} * {dy * M_PER_DEGREE_LAT}",
                "Density X,Y": f"{1 / (dx * M_PER_DEGREE_LON)},{1 / (dy * M_PER_DEGREE_LAT)}",
            }
        )

        Logger.muted = base_mute_status

    @check_file_status
    def read_band(self, band_number: int = 1) -> numpy.ndarray:
        """
        Reads and returns data from a specific band of the open dataset.

        This method extracts data from a specified band number of the dataset currently loaded into the
        instance. It is useful for analyzing specific types of data within a multiband geospatial dataset,
        such as different spectral bands in satellite imagery. The method checks if the dataset is loaded
        (using the @check_file_status decorator) before attempting to read the band, ensuring that the operation
        does not proceed if there is no dataset available.

        Parameters
        ----------
        band_number : int, optional, default=1
            The number of the band to read in the dataset. The default is 1

        Returns
        -------
        numpy.ndarray
            A 2D NumPy array containing the data read from the specified band. The array's data type and
            structure depend on the dataset's format and content.

        Raises
        ------
        IndexError
            If the specified band number is not present in the dataset.

        Examples
        --------
        Assuming 'geo_handler' is an instance of a class containing this method, and a dataset has been successfully
        opened:

            >>> band_data = geo_handler.read_band()
            >>> print(type(band_data))
                <class 'numpy.ndarray'>

        Note
        ----
        The method is decorated with check_file_status, which ensures that the dataset is properly loaded and
        available before attempting to print its information. This helps prevent errors and ensures the availability
        of data.
        """
        return self.dataset.read(band_number)

    @check_file_status
    def read_data(self, band_number: int = 1) -> None:
        """
        Reads data from a specified band and updates the instance's X, Y, Z properties at a specified resolution.

        Parameters
        ----------
        band_number : int, optional, default=1
            The number of the band to read from the dataset. Defaults to 1. Band numbers start at 1 and
            increase with the number of bands in the dataset.

        Note
        ----
        The method is decorated with check_file_status, which ensures that the dataset is properly loaded and
        available before attempting to print its information. This helps prevent errors and ensures the availability
        of data.
        """
        if len(self.x.data) and len(self.y.data) and len(self.z.data):
            Logger.info("Data is already loaded")
            return
        resolution = max(
            1, min(self.resolution_diminution_factor, MAX_ALLOWED_RESOLUTION_DIMINUTION)
        )
        Logger.info("Reading data from file")
        band_data = self.dataset.read(band_number)
        X, Y, Z = [], [], []

        # Adjust loop increments according to the resolution
        for rowIndex in tqdm(
            range(0, band_data.shape[0], resolution),
            desc="Processing shape",
            disable=Logger.muted,
        ):
            for colIndex in range(0, band_data.shape[1], resolution):
                x, y = self.dataset.transform * (colIndex, rowIndex)
                X.append(x)
                Y.append(y)
                Z.append(band_data[rowIndex, colIndex])

        self.x.data = X
        self.y.data = Y
        self.z.data = Z

        self.update()
        Logger.success("File have been read.")

    def update(self) -> None:
        """
        Updates the spatial properties and transformations of the current dataset.

        This method updates the spatial metadata of the instance based on the current state of its
        x, y, and z attributes. It recalculates spatial dimensions in meters and updates the affine
        transformation matrix to reflect the new spatial resolution and extents.

        The method calculates the real-world spatial extents (in meters) of the dataset based on the
        average latitude (for scaling latitude distances) and uses predefined constants or dynamically
        calculated values for meters per degree of latitude and longitude. These calculations are used
        to update the instance's transformation attributes to ensure that spatial operations such as
        mapping pixel coordinates to geospatial coordinates remain accurate.

        The method relies on the x, y, and z attributes having their 'min', 'max', and 'mid' properties
        properly set. If these are not set, the method will not perform the updates correctly.

        Note
        ----
        The method should be called after any operation that changes the spatial data (e.g., reading new
        data or changing the data range). It ensures that all spatial metadata is consistent and up to date.

        Examples
        --------
        Assuming 'geo_handler' is an instance of a class containing this method:

                >>> geo_handler.update()

        This will recalculate and update the spatial dimensions, affine transformation, and resolution
        based on the current data and metadata.
        """
        self.x.update()
        self.y.update()
        self.z.update()
        earth_lat_m_per_degree, earth_lon_m_per_degree = calculate_scale(
            self.y.mid if self.y.mid is not None else BASE_DEGREE
        )
        self.x_meters = (self.x.max - self.x.min) * earth_lon_m_per_degree
        self.y_meters = (self.y.max - self.y.min) * earth_lat_m_per_degree
        self.transform = [
            self.x_meters / self.dataset.width,
            0,
            0,
            0,
            self.y_meters / self.dataset.height,
            0,
        ]
        self.spatial_resolution = (
            self.x_meters / self.dataset.width,
            self.y_meters / self.dataset.height,
        )

    @spatial_data_required
    def create_cloud_point(
        self, scale: CloudPointScalingMode = CloudPointScalingMode.RAW
    ) -> CloudPoint:
        """
        Creates a cloud point object from spatial data extracted from a GeoTIFF file.

        This method generates a CloudPoint object, which represents a point cloud constructed from the dataset.
        The point cloud's scale and density can be adjusted by specifying the scale parameter. The method
        supports multiple scaling modes to accommodate different processing needs and data interpretations.

        Parameters
        ----------
        scale : CloudPointScalingMode, optional
            The scaling mode to apply to the point cloud data extracted from the GeoTIFF. The scaling
            influences how the cloud points are interpreted and represented. Valid options include:
            - RAW: No scaling is applied, data is used as is.
            - NORMALIZED: Data is scaled between 0 and 1.
            - TRUE_SCALE: Data is scaled to represent real-world dimensions accurately.
            By default, the scale is set to RAW.

        Returns
        -------
        CloudPoint
            An instance of the CloudPoint object containing the processed point cloud data. The
            CloudPoint structure and density are determined by the selected scaling mode.


        See Also
        --------
        normalize_scales : Normalizes the scales of x, y, z data points.
        true_scale : Applies real-world scaling to x, y, z data points.
        CloudPoint

        Notes
        -----
        The spatial_data_required decorator ensures that the method is called with the necessary
        spatial data context. This context is typically established by loading a GeoTIFF file
        containing the relevant geographic and spatial information.

        Examples
        --------
        Creating a cloud point with default settings (RAW scale):

            >>> cloud_point = instance.create_cloud_point()
            >>> print(type(cloud_point))
                <class 'CloudPoint'>

        Creating a cloud point with normalized scale:

            >>> cloud_point = instance.create_cloud_point(scale=CloudPointScalingMode.NORMALIZED)
            >>> print(cloud_point.scale)
                'NORMALIZED'
        """
        x, y, z = [], [], []
        if scale == CloudPointScalingMode.NORMALIZED:
            x, y, z = self.normalize_scales()
        elif scale == CloudPointScalingMode.TRUE_SCALE:
            x, y, z = self.true_scale()
        elif scale == CloudPointScalingMode.RAW or scale is None:
            x, y, z = self.x.data, self.y.data, self.z.data

        return CloudPoint(x, y, z, base_scale=scale)

    @spatial_data_required
    @update_spatial_data
    def normalize_scales(self) -> tuple[list[float], list[float], list[float]]:
        """
        Normalizes the spatial scale of the dataset's x, y, and z attributes.

        This method applies normalization to the spatial data contained within the x, y, and z
        attributes of the instance, bringing all values into a standardized range (typically 0 to 1).

        Returns
        -------
        tuple
            A tuple containing the normalized arrays: (x_normalized, y_normalized, z_normalized).
            Each element of the tuple is a NumPy array with the same dimensions as the original data,
            but with values scaled to the range 0 to 1.

        Note
        ----
        The method employs two decorators: @spatial_data_required and @update_spatial_data, which
        ensure that the data exists and is updated respectively before and after normalization.

        Examples
        --------
        Assuming 'geo_handler' is an instance of a class containing this method:

            >>> x_norm, y_norm, z_norm = geo_handler.normalize_scales()
            >>> print(x_norm[0], y_norm[0], z_norm[0])  # Print the first normalized values

        The example demonstrates how to normalize spatial scales and access the normalized data.
        """
        return self.x.normalized, self.y.normalized, self.z.normalized

    @spatial_data_required
    @update_spatial_data
    def true_scale(self) -> tuple[list[float], list[float], list[float]]:
        """
        Converts spatial data coordinates from latitude and longitude to meters.

        This method adjusts the x and y coordinates of the dataset from a latitude/longitude system to a
        metric system (meters), facilitating more realistic and practical spatial analysis and visualization.
        The conversion is based on the real-world metric extents (self.x_meters and self.y_meters) derived from
        the dataset's geographic bounds. The method is crucial for applications requiring accurate distance
        measurements or spatial representations in metric units.

        The z values, representing elevation or height, are assumed to be already in meters and thus are not
        converted. However, the framework for potential z value conversion exists and can be applied if necessary
        for specific datasets.

        Returns
        -------
        tuple
            A tuple containing the arrays of converted x, y, and unchanged z values: (x_converted, y_converted, z).
            Each element in the tuple is a list with values scaled to represent real-world distances in meters.

        Note
        ----
        Ensure the dataset is properly loaded and initialized before calling this method. The conversion relies
        on accurate x and y range values, as well as pre-calculated meter conversion factors (self.x_meters and
        self.y_meters).

        Raises
        ----
        ValueError
            A ValueError is raised if the dataset is invalid for real earth conversion.

        Examples
        --------
        Assuming 'geo_handler' is an instance of a class containing this method:

            >>> x_meters, y_meters, z = geo_handler.true_scale()
            >>> print(x_meters[0], y_meters[0], z[0])  # Print the first true scale values

        This example demonstrates converting spatial scales to true metric scales and accessing the converted data.
        """
        Logger.info("Transform GeoTIFF values to Earth-scale values.")
        if self.x.max > 180 or self.x.min < -180 or self.y.max > 90 or self.y.min < -90:
            error = "The dataset is not suitable to be converted to real earth data. X (longitude) should be between -180 and 180, Y (latitude) should be between -90 and 90."
            Logger.error(error, timestamp=True)

        # Converting data to their true scale, Geotiff stores point using Lat/Long system, so we want to convert them as
        # meters instead for a more realistic visualisation
        def transform_x():
            progress_bar = tqdm(self.x.data, desc="Processing X", disable=Logger.muted)
            xt = []
            for xi in progress_bar:
                transformed_value = (
                    (xi - self.x.min) / (self.x.max - self.x.min) * self.x_meters
                )
                xt.append(transformed_value)
            return xt

        def transform_y():
            progress_bar = tqdm(self.y.data, desc="Processing Y", disable=Logger.muted)
            yt = []
            for yi in progress_bar:
                transformed_value = (
                    (yi - self.y.min) / (self.y.max - self.y.min) * self.y_meters
                )
                yt.append(transformed_value)
            return yt

        def transform_z(transform_factor: bool | float = False):
            zt = []
            if transform_factor:
                progress_bar = tqdm(
                    self.y.data, desc="Processing Z", disable=Logger.muted
                )
                for zi in progress_bar:
                    transformed_value = (
                        (zi - self.z.min) / (self.z.max - self.z.min) * transform_factor
                    )
                    zt.append(transformed_value)
            else:
                Logger.info("Z is ignored in the transformation process.")
                zt = self.z.data
            return zt

        x = transform_x()
        y = transform_y()
        z = transform_z()

        Logger.success("X,Y,Z fully transformed to Earth-scale")
        return x, y, z

    def set_resolution_diminution_factor(self, value=None) -> None:
        if value is None:
            Logger.warning(
                "Invalid resolution diminution factor given. Resetting resolution diminution to 1."
            )
            value = 1
        Logger.info("Updating resolution diminution factor to {}".format(value))
        self.resolution_diminution_factor = max(
            1, min(value, MAX_ALLOWED_RESOLUTION_DIMINUTION)
        )

    def reset(self):
        self.dataset = None
        self.x = SpatialMetrics()
        self.y = SpatialMetrics()
        self.z = SpatialMetrics()
        self.spatial_resolution = None
        self.transform = None
        self.y_meters = None
        self.x_meters = None
        self.resolution_diminution_factor = 1
