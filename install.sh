#!/usr/bin/env bash
#
# Topology project install script
#
PYTHON_EXEC=$( command -v python3 || command -v python)
VERSION=0.1.0

usage() {
    cat << EOF
Install pdp_topology
--------------------

This script installs all the dependencies of pdp_topology

OPTIONS:

-h --help            show this help message and exit
   --venv-directory  define the virtualenv directory (default .venv)
-v --version         show version and exit

EOF
}

version(){
    printf "Install script for pdp_topology V%s" "$VERSION"
}

error() {
    local msg
    msg="$*"
    >&2 printf "\e[31mERROR\e[0m %s\n" "$msg"
}

validate() {
    printf "\e[32mOK\e[0m %s\n" "$msg"
}

die() {
    local msg
    local code
    msg="$1"
    code=${2:1}
    error "$msg"
    exit "$code"
}

check_requirement() {
    printf "Check requirements:\n"
    printf "\tpython executable [%s]: " "$PYTHON_EXEC"
    [[ -z $PYTHON_EXEC ]] && die "not found" 10
    validate
    
    check_python_module "pip"
    check_python_module "venv"
}

check_python_module() {
    local module
    module="$1"
    [[ -z $module ]] && die "check_python_module() need a parameter, non found"
    printf "\tCheck python module %s: " "$module"
    if ! $PYTHON_EXEC -c "help('modules')" 2> /dev/null | grep -q "$module"
    then
        die "not found" 11
    fi
    validate
}

process_args() {
    while :; do
        case ${1:-} in
            -h|-\?|--help)
                usage
                exit 0
                ;;
            --venv-directory)
                VENV_DIR="$2"
                shift
                ;;
            -v|--version)
                version
                exit 0
                ;;
            *)
            break
        esac
        shift
    done
}

prepare_venv() {
    local directory
    directory="$1"
    printf "Install Python venv: "
    if ! "$PYTHON_EXEC" -m venv "$directory" >> install.log 2>> install.log
    then
        die "check install.log" 20
    fi
    validate
}

activate_venv() {
    local directory
    directory="$1"
    printf "Activate venv %s...: " "$directory"
    if ! source "${directory}/bin/activate" >>install.log 2>>install.log
    then
        die "check install.log" 30
    fi
    validate
}

install_dependencies() {
    printf "Install python dependencies: "
    if ! pip install -r ./requirements.txt >>install.log 2>>install.log
    then
        die "check install.log" 40
    fi
    validate
}

main() {
    printf "pdp_topology install script\n" >install.log
    check_requirement
    process_args "$@"
    VENV_DIR=${VENV_DIR:-".venv"}
    prepare_venv "$VENV_DIR"
    activate_venv "$VENV_DIR"
    install_dependencies
}

main "$@"
