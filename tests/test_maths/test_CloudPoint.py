import unittest

import numpy as np
from classes.maths.CloudPoint import CloudPoint
from classes.maths.Point import Point


class TestCloudPoint(unittest.TestCase):
    def setUp(self):
        size = 1_000_000
        self.x = np.arange(size)
        self.y = np.arange(size)
        self.z = np.random.uniform(-200, 200, size)
        self.cloud_point = CloudPoint(self.x.tolist(), self.y.tolist(), self.z.tolist())

    def test_initialization(self):
        x, y, z = self.cloud_point.get_coordinates()

        self.assertIsInstance(self.x, np.ndarray)
        self.assertIsInstance(self.y, np.ndarray)
        self.assertIsInstance(self.z, np.ndarray)

        self.assertTrue(np.array_equal(x, self.x), "X are not equal")
        self.assertTrue(np.array_equal(y, self.y), "Y are not equal")
        self.assertTrue(np.array_equal(z, self.z), "Z are not equal")

    def test_point_count(self):
        self.assertEqual(len(self.cloud_point.points), len(self.x))

    def test_get_coordinates(self):
        x, y, z = self.cloud_point.get_coordinates()

        self.assertListEqual(list(x), list(self.x))
        self.assertListEqual(list(y), list(self.y))
        self.assertListEqual(list(z), list(self.z))

    def test_coordinates(self):
        coords = self.cloud_point.coordinates()
        self.assertEqual(len(coords), len(self.cloud_point.points))

    def test_normalize_data(self):
        self.cloud_point.normalize()
        for point in self.cloud_point.get_points():
            self.assertTrue(isinstance(point, Point))
            is_normalized = all(
                0.0 <= coord <= 1.0 for coord in [point.x, point.y, point.z]
            )
            self.assertTrue(is_normalized, "Normalized data should be between 0 and 1")

    def test_chunk(self):
        chunks = self.cloud_point.chunk()
        self.assertTrue(len(chunks) == 4, "There should be 4 chunks created")

    def test_chunk_loss(self):
        chunks = self.cloud_point.chunk()
        point_sum = 0
        for chunk in chunks:
            point_sum += chunk.size
        self.assertTrue(
            self.cloud_point.size == point_sum,
            "The sum of the number of points of every chunk should be equal to the number of points of the original cloud.",
        )

    def test_deep_chunk(self):
        for i in range(1, 3):
            chunks = self.cloud_point.deep_chunk(level=i)
            self.assertTrue(
                len(chunks) == 4**i, f"There should be {4 ** i} chunks created"
            )

    def test_deep_chunk_loss(self):
        for i in range(1, 3):
            chunks = self.cloud_point.deep_chunk(level=i)
            point_sum = 0
            for chunk in chunks:
                point_sum += chunk.size
            self.assertTrue(
                self.cloud_point.size == point_sum,
                "The sum of the number of points of every chunk should be equal to the number of points of the original cloud.",
            )

    def test_slice_x(self):
        slices = 5
        chunks = self.cloud_point.slice(slices=slices, axis="X")
        self.assertTrue(len(chunks) == slices)

        for chunk in chunks:
            self.assertEqual(chunk.size, self.cloud_point.size / slices)

    def test_slice_y(self):
        slices = 5
        chunks = self.cloud_point.slice(slices=slices, axis="Y")
        self.assertTrue(len(chunks) == slices)

        for chunk in chunks:
            self.assertEqual(chunk.size, self.cloud_point.size / slices)

    def test_assemble(self):
        chunks = self.cloud_point.chunk()

        assembled_cloud = CloudPoint.assemble(chunks)

        self.assertEqual(self.cloud_point, assembled_cloud)

    def test_from_points(self):
        points = self.cloud_point.get_points()
        new_cloud = CloudPoint.from_points(points)
        self.assertEqual(self.cloud_point, new_cloud)


if __name__ == "__main__":
    unittest.main()
