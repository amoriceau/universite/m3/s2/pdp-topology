import math

from utils.constants import EARTH_RADIUS_KM


def calculate_scale(lat: float | int) -> tuple[float, float]:
    """
    Calculates the scale factors for converting between degrees of latitude and longitude
    and meters at a given latitude.

    The length per degree of latitude remains constant, but the length per degree of longitude
    varies depending on the latitude. This function calculates these lengths based on the
    provided latitude and the Earth's radius.

    Parameters
    ----------
    lat : float
        The latitude (in degrees) at which to calculate the scale factors.

    Returns
    -------
    tuple[float, float]
        A tuple containing the number of meters per degree of latitude and the number of meters
        per degree of longitude at the specified latitude.

    Examples
    --------
    >>> scale_lat, scale_lon = calculate_scale(45.0)
    >>> print(scale_lat, scale_lon)

    Notes
    -----
    The Earth's radius is assumed to be 6378.137 kilometers.
    """
    lat_rad = math.radians(lat)
    m_per_degree_lat = (EARTH_RADIUS_KM * math.pi / 180) * 1_000

    # Calculer la longueur en km d'un degré de longitude (varie avec la latitude)
    m_per_degree_lon = (EARTH_RADIUS_KM * math.cos(lat_rad) * math.pi / 180) * 1_000

    return m_per_degree_lat, m_per_degree_lon
