from matplotlib import cm

from classes.enums.CloudPointScalingMode import CloudPointScalingMode
from classes.enums.MeshExportType import MeshExportType
from classes.handlers.GeoTIFFHandler import GeoTIFFHandler
from classes.handlers.XYZHandler import XYZHandler
from classes.maths.CloudPoint import CloudPoint
from classes.maths.Mesh import Mesh
from classes.utils.Logger import Logger
from classes.utils.Visualizer import Visualizer
from src.classes.handlers.LASHandler import LASHandler


def main():
    # Create CloudPoints
    # from_geotiff(CloudPointScalingMode.RAW)
    # from_geotiff(CloudPointScalingMode.NORMALIZED)
    # from_geotiff(CloudPointScalingMode.TRUE_SCALE)

    # Optional run, adjusted resolution of the Cloud
    # from_geotiff(CloudPointScalingMode.TRUE_SCALE, 2)
    # from_geotiff(CloudPointScalingMode.TRUE_SCALE, 5)
    # from_geotiff(CloudPointScalingMode.TRUE_SCALE, 10)
    # from_xyz()

    # Export CloudPoint
    # export_cloud_xyz()

    # Playing with clouds
    # cp = from_xyz()
    # print(cp.get_grid())
    # chunk_cloud(cp, 2)
    # cps = chunk_cloud_alt(cp, 1)
    # slice_cloud(cp)
    # assemble_clouds(cps)
    cp = from_geotiff(CloudPointScalingMode.TRUE_SCALE, 1)
    my_mesh = Mesh(cp)
    my_mesh.create_triangles()
    my_mesh.export("test.obj", MeshExportType.OBJECT)


def from_geotiff(scale=None, resolution_diminution_factor=None, visualize=True):
    if scale is None:
        scale = CloudPointScalingMode.TRUE_SCALE
    # Files are in data/geotiff
    # files = ["FUJI_ASTGTMV003_N35E138_dem.tif", "BREST_ASTGTMV003_N48W005_dem.tif"]
    files = ["FUJI_ASTGTMV003_N35E138_dem.tif"]

    cp = None

    for file in files:
        geo_handler = GeoTIFFHandler(f"../data/geotiff/{file}")
        geo_handler.open()
        geo_handler.print_info()
        geo_handler.set_resolution_diminution_factor(resolution_diminution_factor)
        cp = geo_handler.create_cloud_point(scale=scale)
        plot_name = (
            file
            + f" resolution diminution_factor={geo_handler.resolution_diminution_factor}x"
        )
        geo_handler.close()
        if visualize:
            Visualizer().visualize(
                cloud_point=cp,
                cmap=cm.rainbow,
                name=plot_name,
                rescale_box=CloudPointScalingMode.TRUE_SCALE == scale,
            )

    return cp


def from_xyz():
    file = "FUJI_ASTGTMV003_N35E138_dem.xyz"
    # file = "FUJI_BOTTOM_RIGHT.xyz"
    xyzHandler = XYZHandler(f"data/generation/xyz/{file}")
    cp = xyzHandler.create_cloud_point()
    Visualizer().visualize(cloud_point=cp, cmap=cm.rainbow, name=file)
    return cp


def export_cloud_xyz():
    cp = from_geotiff()
    cp.to_xyz("exported_cloud_point")


def chunk_cloud(cloud, level=1):
    if level == 0:
        return [cloud]

    chunks = cloud.chunk()

    for i, chunk in enumerate(chunks):
        Visualizer().visualize(
            cloud_point=chunk, cmap=cm.inferno, name=f"chunk_{level}_{i}"
        )

    sub_chunks = []
    for chunk in chunks:
        sub_chunks.extend(chunk_cloud(chunk, level - 1))

    return sub_chunks


def chunk_cloud_alt(cloud, level=1):
    Visualizer().visualize(cloud_point=cloud, cmap=cm.rainbow, name=f"Full cloud")
    chunks = cloud.deep_chunk(level=level)

    for i, chunk in enumerate(chunks):
        Visualizer().visualize(
            cloud_point=chunk, cmap=cm.rainbow, name=f"cloud_chunk_{i}"
        )
    return chunks


def slice_cloud(cloud):
    slices_y = cloud.slice(3, "Y")
    slices_x = cloud.slice(10, "X")

    y = 0
    for c_slice in slices_y:
        Visualizer().visualize(cloud_point=c_slice, cmap=cm.rainbow, name=f"slice_{y}")
        y += 1

    x = 0
    for c_slice in slices_x:
        Visualizer().visualize(cloud_point=c_slice, cmap=cm.viridis, name=f"slice_{x}")
        x += 1


def assemble_clouds(clouds):
    assembled_cloud = CloudPoint.assemble(clouds)
    Visualizer().visualize(
        cloud_point=assembled_cloud, cmap=cm.rainbow, name="Assembled cloud}"
    )


if __name__ == "__main__":
    Logger.configure_logger()
    # main()
    las_handler = LASHandler("../data/las/laz/UJ.laz")

    las_handler.open()
    las_handler.read_points()

    cloud_point = las_handler.create_cloud_point()
    las_handler.close()

    cps = cloud_point.deep_chunk(level=2)
    i = 1
    for cp in cps:
        cp.to_xyz(f"uj_chunk_{i}")
        i = i + 1
    # cloud_point.to_xyz("uj_cloud")
    # Visualizer().visualize(cloud_point=cloud_point, cmap=cm.RdPu, name="Test LAZ")

    my_mesh = Mesh(cp)
    my_mesh.create_triangles()
    # my_mesh.export('test.obj',MeshExportType.OBJECT)
