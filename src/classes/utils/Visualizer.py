from typing import Any

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from scipy.interpolate import griddata

from classes.enums.Visualizations import Visualizations
from classes.maths.CloudPoint import CloudPoint
from classes.utils.Logger import Logger

from src.classes.enums.Visualizations import Visualizations
from src.classes.maths.CloudPoint import CloudPoint
from src.classes.utils.Logger import Logger


class Visualizer:
    def visualize(
        self,
        cloud_point: CloudPoint,
        cmap: cm.ColormapRegistry = cm.rainbow,
        interpolate: bool = False,
        plots: list[Visualizations] = None,
        name: str = None,
        rescale_box: bool = True,
    ) -> None:
        """
        Visualizes the provided CloudPoint instance using specified settings.

        Parameters
        ----------
        cloud_point : CloudPoint
            The CloudPoint instance containing the data to be visualized.
        cmap : matplotlib Colormap, optional
            The colormap to use for the visualization. Defaults to matplotlib's rainbow colormap.
        interpolate : bool, optional
            Whether to interpolate the data onto a regular grid. Interpolation may enhance
            the visual appeal for sparse datasets but can be computationally intensive.
            Defaults to False.
        plots : list of Visualizations, optional
            A list of Visualizations enums indicating which types of plots to generate
            (e.g., [Visualizations.TWO_DIMENSIONS, Visualizations.THREE_DIMENSIONS]).
            Defaults to [Visualizations.THREE_DIMENSIONS] if None.
        name : str, optional
            An optional name for the dataset to be included in the plot title.
            Defaults to None.
        rescale_box : bool, optional
            Whether to rescale the 3D plot box to fit the data. Defaults to True.

        Examples
        --------
        Assuming x,y,z are valid data to create a CloudPoint

        >>> visualizer = Visualizer()
        >>> cloud_point = CloudPoint(x,y,z)
        >>> visualizer.visualize(cloud_point, interpolate=True, plots=[Visualizations.TWO_DIMENSIONS, Visualizations.THREE_DIMENSIONS])

        Notes
        -----
        The 3D visualization uses a surface plot, while the 2D visualization uses a contour plot.
        """
        if plots is None or len(plots) == 0:
            plots = [Visualizations.THREE_DIMENSIONS]

        Logger.info("Retrieving x,y,z points from cloud point.")
        x, y, z = cloud_point.get_coordinates(clean=True)
        z_max = np.max(z)
        contour_interval = z_max // 10

        try:
            Logger.info("Preparing data for visualization.")
            if (
                interpolate
            ):  # Warning this is time/resource consuming, only interpolate when truly needed.
                Logger.info("Interpolating data onto a regular grid")
                X, Y, Z = self.interpolate_data(x, y, z)
            else:
                Logger.info("Assuming x, y, z are already structured for plotting...")
                X, Y = np.meshgrid(sorted(set(x)), sorted(set(y)))
                Logger.info("X,Y reshaped!")
                Z = z.reshape(len(set(y)), len(set(x)))
                Logger.info("Z reshaped!")
        except Exception as e:
            Logger.error("Error while retrieving the points from cloud")
            Logger.error(str(e))
            raise e

        Logger.info("Creating figure")
        fig = plt.figure(figsize=(6 * len(plots), 6))

        try:
            for i, plot_type in enumerate(plots):
                if plot_type == Visualizations.THREE_DIMENSIONS:
                    Logger.info("Creating 3D visualization...")
                    ax = fig.add_subplot(1, len(plots), i + 1, projection="3d")
                    ax.plot_surface(X, Y, Z, cmap=cmap, linewidth=0, antialiased=True)
                    ax.set_title("3D Surface Visualization")
                    ax.set_zlabel("Z")
                    ax.set_xlim(min(x), max(x))
                    ax.set_ylim(min(y), max(y))
                    ax.set_zlim(0)
                    ax.set_zlim(np.min(Z), np.max(Z))

                    if rescale_box:
                        # Determining the smallest aspect-ratio for a good visualization
                        x_range = max(x) - min(x)
                        y_range = max(y) - min(y)
                        z_range = np.max(Z) - np.min(
                            Z
                        )  # Z is already adjusted for the grid, so we save computation time
                        min_range = min(x_range, y_range, z_range)
                        aspect_ratio = [
                            x_range / min_range,
                            y_range / min_range,
                            z_range / min_range,
                        ]

                        ax.set_box_aspect(aspect_ratio)
                    else:
                        ax.set_aspect("equal")
                        ax.set_box_aspect([1, 1, 1])
                    Logger.info("3D visualization ready.")

                elif plot_type == Visualizations.TWO_DIMENSIONS:
                    Logger.info("Creating 2D visualization...")
                    ax = fig.add_subplot(1, len(plots), i + 1)
                    contour = ax.contourf(
                        X,
                        Y,
                        Z,
                        levels=np.arange(np.min(Z), np.max(Z), contour_interval),
                        cmap=cmap,
                    )
                    fig.colorbar(contour, ax=ax, shrink=0.5, aspect=5, label="Altitude")
                    ax.set_title("2D Contour Visualization")
                    Logger.info("2D visualization ready.")

                ax.set_xlabel("X")
                ax.set_ylabel("Y")

            Logger.info("Displaying the visualization")
            points = "{:,}".format(len(X) * len(Y)).replace(",", " ")
            title = "Visualization of " + (
                f"{name} ({points} points)" if name else f"{points} points"
            )
            fig.suptitle(title, fontsize=8)
            plt.tight_layout()
            plt.show()
        except Exception as e:
            Logger.error("Error in visualization function", throws=False)
            Logger.error(str(e))

    @staticmethod
    def interpolate_data(
        x: list[float], y: list[float], z: list[float]
    ) -> tuple[Any, Any, Any]:
        """
        Interpolates unstructured (x, y, z) data onto a regular grid.

        Parameters
        ----------
        x : list of float
            The x-coordinates of the data points.
        y : list of float
            The y-coordinates of the data points.
        z : list of float
            The z-values (heights) at each data point.

        Returns
        -------
        tuple of np.ndarray
            A tuple containing the meshgrid arrays (X, Y) and the interpolated z-values (Z) on the regular grid.

        Examples
        --------
        >>> x, y, z = [1, 2, 3], [4, 5, 6], [7, 8, 9]
        >>> X, Y, Z = Visualizer.interpolate_data(x, y, z)

        Warnings
        --------
        This method is useful for plotting and generate a better representation of the data, but it has a high cpu
        consumption and requires a lot of memory and time to process.
        """
        xi = np.linspace(min(x), max(x), 100)
        yi = np.linspace(min(y), max(y), 100)
        X, Y = np.meshgrid(xi, yi)
        Z = griddata((x, y), z, (X, Y), method="linear")
        return X, Y, Z
