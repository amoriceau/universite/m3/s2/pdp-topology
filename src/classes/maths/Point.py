from __future__ import annotations


class Point:
    """
    A class representing a point in three-dimensional space.

    Attributes
    ----------
    id : int or str
        An identifier for the point, which can be of any type but is typically an integer or string.
    x : float
        The x-coordinate of the point in 3D space.
    y : float
        The y-coordinate of the point in 3D space.
    z : float, optional
        The z-coordinate of the point in 3D space. Defaults to 0.0.

    Methods
    -------
    def coord(self) -> list[float]:
        Returns the coordinates of the point in 3D space.
    __mul__(other)
        Supports multiplication of the point by a scalar (int or float) or a 3-tuple representing scale factors along each axis.
    

    Examples
    --------
    >>> p = Point(1, 2.0, 3.0, 4.0)
    >>> print(p)
    Point(id=1, x=2.0, y=3.0, z=4.0)
    >>> q = p * 2
    >>> print(q)
    Point(id=1, x=4.0, y=6.0, z=8.0)
    >>> r = p * (1, 2, 3)
    >>> print(r)
    Point(id=1, x=2.0, y=6.0, z=12.0)

    Notes
    -----
    The Point class is designed to represent spatial coordinates and support basic geometric transformations.
    """

    def __init__(self, identifier: int, x: float, y: float, z: float = 0.0):
        self.x = x
        self.y = y
        self.z = z
        self.id = identifier

    def coordinates(self) -> list[float]:
        """
        Returns the coordinates of the point in 3D space

        Returns
        -------
        list[float, float, float]
        """
        return [self.x, self.y, self.z]

    def __mul__(self, other) -> Point:
        if isinstance(other, (int, float)):  # Multiplication par un scalaire uniquement
            return Point(self.id, self.x * other, self.y * other, self.z * other)
        elif isinstance(other, tuple) and len(other) == 3:
            # Multiplication par un tuple (x, y, z)
            return Point(
                self.id, self.x * other[0], self.y * other[1], self.z * other[2]
            )
        else:
            raise ValueError("Can only multiply by int or float")

    def __eq__(self, other):
        if not isinstance(other, Point):
            return NotImplemented
        return (
            (self.id == other.id)
            and (self.x == other.x)
            and (self.y == other.y)
            and (self.z == other.z)
        )

    def __repr__(self) -> str:
        return f"Point(id={self.id}, x={self.x}, y={self.y}, z={self.z})"
    
    
