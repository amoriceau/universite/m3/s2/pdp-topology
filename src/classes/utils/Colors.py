class Colors:
    # Colors definitions
    HEADER = "\033[95m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    WARNING = "\033[93m"
    GRAY = "\033[90m"
    FAIL = "\033[91m"

    # Styles definitions
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"  # Mandatoryto remove the previously applied styles
