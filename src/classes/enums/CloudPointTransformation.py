from enum import Enum


class CloudPointTransformation(Enum):
    NONE = None
    CHUNKED = "chunked"
    SLICED_X = "sliced_x"
    SLICED_Y = "sliced_y"
