import os

import laspy
import numpy as np

from src.classes.enums.CloudPointScalingMode import CloudPointScalingMode
from src.classes.maths.CloudPoint import CloudPoint
from src.classes.maths.SpatialMetrics import SpatialMetrics
from src.classes.utils.Logger import Logger


class LASHandler:
    """
    A handler class for LAS and LAZ files used for LiDAR data processing.

    This class provides utilities for opening, reading, and processing data from LAS and LAZ files,
    commonly used in LiDAR surveying and remote sensing. It supports reading LiDAR point cloud data,
    and converting point cloud coordinates into real-world metrics.

    Attributes
    ----------
    filepath : str
        Path to the LAS or LAZ file to be handled.
    filename : str
        Name of the file without path and extension.
    file : laspy.file.File
        The opened file object from laspy library, representing the LAS or LAZ file.
    points : numpy.ndarray
        Array of point records from the file.
    x, y, z : SpatialMetrics
        Spatial metrics for the file, including min, max, and data values for each axis.

    Methods
    -------
    open()
        Opens the LAS or LAZ file and initializes the file object.
    close()
        Closes the opened LAS or LAZ file and releases resources.
    read_points()
        Reads point records from the file and updates the instance's X, Y, Z properties.
    create_cloud_point(scale=CloudPointScalingMode.RAW)
        Generates a cloud point object from the LiDAR point cloud data.
    """

    def __init__(self, filepath: str):
        self.filepath = filepath
        self.filename = os.path.basename(filepath).split(".", 1)[0]
        self.file = None
        self.points = None
        self.x = SpatialMetrics()
        self.y = SpatialMetrics()
        self.z = SpatialMetrics()

    def open(self) -> None:
        """
        Opens the LAS or LAZ file and initializes the file object.
        """
        self.file = laspy.read(self.filepath)
        Logger.success(f"{self.filename} has been opened successfully.")

    def close(self) -> None:
        """
        Closes the opened LAS or LAZ file and releases resources.
        """
        # In laspy, the files are read-only so there's no need to close the file explicitly.
        # But we nullify the variables to release the memory.
        self.file = None
        self.points = None
        Logger.success(f"{self.filename} has been closed successfully.")

    def read_points(self) -> None:
        """
        Reads point records from the LAS or LAZ file and updates the instance's X, Y, Z properties.
        """
        if self.file is None:
            Logger.error("File is not opened")
            return

        print(self.file.x)
        print(self.file.y)
        print(self.file.z)

        self.points = np.vstack((self.file.x, self.file.y, self.file.z)).transpose()
        self.x.data = self.file.x
        self.y.data = self.file.y
        self.z.data = self.file.z

        Logger.success("Point data have been read.")

    def create_cloud_point(self, scale=CloudPointScalingMode.RAW) -> CloudPoint:
        """
        Generates a cloud point object from the LiDAR point cloud data.
        """
        if self.points is None:
            Logger.error("Point data is not available")
            return None

        if scale == CloudPointScalingMode.RAW:
            return CloudPoint(self.x.data, self.y.data, self.z.data, base_scale=scale)
        else:
            Logger.error("Currently only RAW scale is supported for LAS files.")
            return None
