import numpy as np


class SpatialMetrics:
    def __init__(self, data=None):
        """
        A class for handling and computing spatial metrics for a dataset.

        This class provides functionalities for calculating and storing statistical properties
        of spatial data, such as size, minimum, maximum, range, normalized values, and the midpoint.

        Attributes
        ----------
        data : np.array
            The dataset from which spatial metrics are computed. Should be a NumPy array.

        Methods
        -------
        update():
            Recalculates the metrics based on the current data. This method should be called whenever the data changes.

        Examples
        --------
        >>> metrics = SpatialMetrics(data=np.array([1, 2, 3, 4, 5]))
        >>> metrics.update()
        >>> print(metrics)
        SpatialMetrics(size=5, min=1, max=5, mid=3.0, range=4)

        See Also
        --------
        update: Updates the computed properties of the object.
        size : int or None
            The number of elements in the dataset. Set to None before update is called.
        min : float or None
            The minimum value in the dataset. Set to None if the dataset is empty.
        max : float or None
            The maximum value in the dataset. Set to None if the dataset is empty.
        range : float or None
            The range of the dataset (max - min). Set to None if the dataset is empty.
        normalized : np.array or None
            The dataset normalized to a range between 0 and 1. Set to the original data if the range is None.
        mid : float or None
            The midpoint value of the dataset ((min + max) / 2.0).

        Notes
        -----
        The `update` method must be called after initializing the class or changing the data to recalculate the metrics.
        """
        if data is None:
            data = []
        self.data = np.array(data)
        self.size = None
        self.min = None
        self.max = None
        self.range = None
        self.normalized = None
        self.mid = None

    def update(self) -> None:
        """
        Recalculates and updates the spatial metrics based on the current dataset.

        This method computes the size, minimum, maximum, range, and normalized values of the data,
        as well as the midpoint value. It updates the instance attributes accordingly. This method
        should be called whenever the data is modified to ensure that all metrics are current.

        The normalization process adjusts the data such that it spans a range from 0 to 1 based on
        the minimum and maximum values. If the dataset is empty, all metrics are set to None except
        size, which is set to zero.

        """
        self.size = len(self.data)
        self.min = np.min(self.data) if len(self.data) > 0 else None
        self.max = np.max(self.data) if len(self.data) > 0 else None
        self.range = self.max - self.min if self.min is not None else None
        self.normalized = (self.data - self.min) / self.range if self.range else None
        self.mid = (self.min + self.max) / 2.0 if self.size > 0 else None

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"size={self.size}, "
            f"min={self.min}, "
            f"max={self.max}, "
            f"mid={self.mid})"
            f"range={self.range}, "
        )
