from enum import Enum


class MeshExportType(Enum):
    OBJECT = "obj"
    STEREOLITHOGRAPHY = "stl"
    POLYGONS = "ply"
