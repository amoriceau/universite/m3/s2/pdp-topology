import datetime
import inspect
import logging
import os
import re

from classes.utils.Colors import Colors


class Logger:
    in_file = True
    full_timestamp = True
    muted = False
    log_level = logging.INFO

    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)

    class CustomFormatter(logging.Formatter):
        grey = Colors.GRAY
        blue = Colors.BLUE
        green = Colors.GREEN
        yellow = Colors.WARNING
        red = Colors.FAIL
        bold_red = Colors.BOLD + Colors.FAIL
        reset = Colors.END
        format = f"[{Colors.GRAY}%(asctime)s{Colors.END}]%(message)s"

        FORMATS = {
            logging.DEBUG: format + reset,
            logging.INFO: format + reset,
            logging.WARNING: format + reset,
            logging.ERROR: format + reset,
            logging.CRITICAL: format + reset,
        }

        def format(self, record):
            log_fmt = self.FORMATS.get(record.levelno)
            formatter = logging.Formatter(log_fmt, "%Y-%m-%d %H:%M:%S")
            return formatter.format(record)

    class CleanFormatter(logging.Formatter):
        def format(self, record):
            original = super().format(record)
            return Logger._clean_ansi_codes(original)

    @staticmethod
    def configure_logger(muted=False, in_file=True, full_timestamp=True):
        Logger.muted = muted
        Logger.in_file = in_file
        Logger.full_timestamp = full_timestamp

        Logger.logger = logging.getLogger("CustomLogger")
        Logger.logger.setLevel(Logger.log_level)

        # Console handler
        ch = logging.StreamHandler()
        ch.setLevel(Logger.log_level)
        ch.setFormatter(Logger.CustomFormatter())
        Logger.logger.addHandler(ch)

        if Logger.in_file:
            log_file_name = Logger._get_log_file_name()
            fh = logging.FileHandler(log_file_name)
            fh.setLevel(Logger.log_level)
            # Clean formatter is used to remove ANSI codes from the log file.
            fh.setFormatter(
                Logger.CleanFormatter("[%(asctime)s]%(message)s", "%Y-%m-%d %H:%M:%S")
            )
            Logger.logger.addHandler(fh)

    @staticmethod
    def _clean_ansi_codes(text: str) -> str:
        ansi_escape = re.compile(r"\x1B[@-_][0-?]*[ -/]*[@-~]")
        return ansi_escape.sub("", text)

    @staticmethod
    def _get_log_file_name() -> str:
        project_root = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "..", "..", "..")
        )
        logs_directory = os.path.join(project_root, "logs")

        if not os.path.exists(logs_directory):
            os.makedirs(logs_directory)

        today = datetime.date.today()
        log_file_name = f"{today.strftime('%Y_%m_%d')}.log"

        return os.path.join(logs_directory, log_file_name)

    @staticmethod
    def info(message: str, title: str = "INFO"):
        if not Logger.muted:
            frame = inspect.stack()[1]
            filename = os.path.basename(frame.filename)
            lineno = frame.lineno

            format_title = Logger._format_title(title=title.upper(), color=Colors.BLUE)
            Logger.logger.info(f"[{format_title}] {message} ({filename}:{lineno})")

    @staticmethod
    def error(
        message: str, title: str = "ERROR", throws: bool = True, throw_type=ValueError
    ):
        if not Logger.muted:
            frame = inspect.stack()[1]
            filename = os.path.basename(frame.filename)
            lineno = frame.lineno

            format_title = Logger._format_title(title=title.upper(), color=Colors.FAIL)
            Logger.logger.error(f"[{format_title}] {message} ({filename}:{lineno})")

        if throws:
            if throw_type is None:
                throw_type = ValueError
            raise throw_type(message)

    @staticmethod
    def warning(message: str, title: str = "WARNING"):
        if not Logger.muted:
            frame = inspect.stack()[1]
            filename = os.path.basename(frame.filename)
            lineno = frame.lineno

            format_title = Logger._format_title(
                title=title.upper(), color=Colors.WARNING
            )
            Logger.logger.warning(f"[{format_title}] {message} ({filename}:{lineno})")

    @staticmethod
    def success(message: str, title: str = "SUCCESS"):
        if not Logger.muted:
            frame = inspect.stack()[1]
            filename = os.path.basename(frame.filename)
            lineno = frame.lineno

            format_title = Logger._format_title(title=title.upper(), color=Colors.GREEN)
            Logger.logger.info(f"[{format_title}] {message} ({filename}:{lineno})")

    @staticmethod
    def _format_title(title, color=Colors.GREEN):
        return f"{color}{title}{Colors.END}"

    @staticmethod
    def _get_source():
        frame = inspect.stack()[1]
        filename = os.path.basename(frame.filename)
        lineno = frame.lineno

        return f"({filename}:{lineno})"

    @staticmethod
    def table(data):
        if not isinstance(data, dict):
            print("Invalid data: Must be a dictionary")
            return

        headers = data.keys()
        column_widths = {
            header: max(len(header), len(str(value))) for header, value in data.items()
        }

        separator_line = (
            "+-" + "-+-".join("-" * column_widths[header] for header in headers) + "-+"
        )

        header_line = (
            "| "
            + " | ".join(
                f"{Colors.BOLD}{Colors.WARNING}{header:{column_widths[header]}}{Colors.END}"
                for header in headers
            )
            + " |"
        )
        print(separator_line)
        print(header_line)
        print(separator_line)

        value_line = (
            "| "
            + " | ".join(
                f"{str(data[header]):{column_widths[header]}}" for header in headers
            )
            + " |"
        )
        print(value_line)
        print(separator_line)
