# PdP Topology

Projet de programmation M1 Université de Bordeaux. Pour le moment ce n'est qu'un
PoC afin de parser les fichier GeoTIFF et d'afficher une représentation via
Mathplotlib.

## Installation

La première chose à faire est de cloner le dépôt:

```shell
$ git clone https://gitlab.emi.u-bordeaux.fr/amoriceau/pdp-topology
```

Le projet est livré avec un script d'installation qui va se charger d'installer
les dépendences. Ce scrupt `install.sh` est fourni avec une aide en ligne via la
commande suivante:

```shell
./install.sh --help
```

Pour installer les dépendances dans le dossier du projet :

```shell
./install.sh
```

### Installation manuelle dans un environnement virtuel Python

Afin d'utiliser les élément du projet, vous pouvez utiliser un environnement
virtuel Python manuellement. Dans un terminal commencez par cloner le dépot :

```shell
$ git clone https://gitlab.emi.u-bordeaux.fr/amoriceau/pdp-topology
```

Puis de mettre en place l'environnement virtuel :

```shell
$ cd pdp-topology
$ python -m venv .venv
$ source .venv/bin/activate
(venv) $ pip install -r requirements.txt
```
Pour Windows :
```shell
$ cd .\pdp-topology\
$ python -m venv .venv
$ .\.venv\Scripts\activate 
(venv) $ pip install -r .\requirements.txt
```

`pip` va alors installer les dépendances du projet.

Attention, vous aurez besoin des bibliothèques `zlib` et `libstdc++6` si vous
utilisez *l'environnement virtuel*. Voici les paquets à installer sur un système
Debian ou dérivés :

```
# apt install zlib libstdc++6
```
### autres méthodes d'installation

#### Installer les dépendances via le gestionnaire de paquets (Linux)

Vous pouvez installer les différentes dépendences que ce soit avec le 
gestionnaire de paquet de votre système. Par exemple sur Debian / Ubuntu et ses
dérivés (en super-utilisateur) :

```
# apt install python-matplotlib python-numpy python-rasterio python-scipy
```

#### Installation avec Nix (développement)

Il est possible d'utiliser le gestionnaire de paquet nix et la commande
`nix-shell` afin d'installer les dépendances grâce au fichier shell.nix fourni.
Ceci permet la mise en place rapide d'un environnement de développement:

```shell
$ cd pdp-topology
$ nix-sell shell.nix
```

Il est aussi possible, pour les utilisateur de `direnv` de lancer
automatiquement le passagle dans le shell nix en exécutant la commande suivante
à la racine du projet:

```shell
$ echo "use nix" > ".envrc"
```

## Lancement de l'exemple

Une fois les dépendance installées il sufit de lancer l'exemple:

```
$ cd src
$ python main.py
Nom du fichier : FUJI_ASTGTMV003_N35E138_dem.tif
Nombre de bandes : 1
Dimensions : 3601 x 3601
Système de coordonnées : EPSG:4326
Transform : | 0.00, 0.00, 138.00|
| 0.00,-0.00, 36.00|
| 0.00, 0.00, 1.00|
# [...]
```
