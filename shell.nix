with (import <nixpkgs> {});
mkShell {
  buildInputs = with pkgs; [
    python311
  ] ++ ( with pkgs.python311Packages; [
    ## dev dependencies
    pylint
    black

    ## runtime dependencies
    numpy
    rasterio
    matplotlib
    scipy
    tqdm
  ]);
  shellHook = ''
    echo '      __                   ___   __                  '
    echo '     / /_____  ____  ____ |__ \ / /_____  ____  ____ '
    echo '    / __/ __ \/ __ \/ __ \__/ // __/ __ \/ __ \/ __ \'
    echo '   / /_/ /_/ / /_/ / /_/ / __// /_/ /_/ / /_/ / /_/ /'
    echo '   \__/\____/ .___/\____/____/\__/\____/ .___/\____/ '
    echo '           /_/                        /_/            '
    echo '                          ---                        '
    echo '               Development environment               '
    echo ' '
  '';
}
